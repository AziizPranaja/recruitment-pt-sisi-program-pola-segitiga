package main

import "fmt"

func main() {
	for {
		var n int
		fmt.Print("Input: ")
		_, err := fmt.Scan(&n)
		if err != nil {
			fmt.Println("Input harus berupa angka.")
			continue
		}
		fmt.Scanln(&n)

		for i := 1; i <= n; i++ {
			for j := 1; j <= n-i; j++ {
				fmt.Print(" ")
			}
			for k := 1; k <= i; k++ {
				fmt.Print("* ")
			}
			fmt.Println()
		}

		fmt.Println()
		fmt.Print("Lanjutkan (Y/N)? ")
		var answer string
		fmt.Scan(&answer)
		if answer == "N" || answer == "n" {
			break
		}
	}
}